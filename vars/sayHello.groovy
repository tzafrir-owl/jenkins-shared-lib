/** javadoc explaining the step
 */
def call(Map config) {
    // Any valid steps can be called from this code, just like in other
    // Scripted Pipeline
    echo "Hello, ${config.name}."
}

// some comment
/* comment call */
def call(String name) {
    // Any valid steps can be called from this code, just like in other
    // Scripted Pipeline
    echo "Hello, name."
}